#!/bin/bash
sleep 5s
if curl web:5000 | grep -q 'Hello Vlad'; then
  echo "Tests passed!"
  exit 0
else
  echo "Tests failed!"
  exit 1
fi
